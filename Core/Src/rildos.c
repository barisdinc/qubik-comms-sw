/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rildos.h"
#include "lfsr.h"
#include "error.h"

/**
 * Creates the RILDOS spreading sequences I and Q.
 *
 * @note To balance space usage
 * and complexity, the I sequence is stored at the MS bit (bit 7) and the Q
 * sequence at the LS bit (bit 0). If the modulation is BPSK, only the I channel
 * is used.
 *
 * @param out a buffer of 2047 bytes to hold the I and Q sequences
 * @param seed the seed for the B LFSR. This should be a Gold sequence seed.
 */
void
rildos_init(uint8_t *out, uint32_t seed)
{
	struct lfsr a = {.sr = 0x33, .mask = 0x101, .len = 10};
	struct lfsr b = {.sr = seed, .mask = 0x149, .len = 10};
	struct lfsr c = {.sr = 0x5a, .mask = 0x101, .len = 10};

	for (uint16_t idx = 0; idx < 2047; idx++) {
		uint8_t out_a = lfsr_shift(&a);
		uint8_t out_b = lfsr_shift(&b);
		uint8_t out_c = lfsr_shift(&c);
		uint8_t i = out_a ^ out_b;
		uint8_t q = out_b ^ out_c;
		out[idx] = (i << 7) | q;
	}
}

uint32_t
rildos_create_frame(uint8_t *out, uint32_t svn, uint32_t uptime_secs)
{
	uint32_t idx = 0;
	/* SFD */
	out[idx++] = 0xBA;
	out[idx++] = 0xDC;
	out[idx++] = 0xAB;
	/* SVN */
	out[idx++] = svn >> 16;
	out[idx++] = svn >> 8;
	out[idx++] = svn;
#if 0
	/* Version */
	out[idx++] = 0;
	/* Timestamp */
	out[idx++] = 0;
	out[idx++] = 0;
	out[idx++] = 0;
	out[idx++] = 0;
#endif
	out[idx++] = uptime_secs >> 24;
	out[idx++] = uptime_secs >> 16;
	out[idx++] = uptime_secs >> 8;
	out[idx++] = uptime_secs;
	return idx;
}

static size_t
spread_bpsk(uint8_t *out, const uint8_t *in, const uint8_t *spread_seq,
            size_t len)
{
	uint8_t sr = 0;
	uint8_t sr_cnt = 0;
	size_t out_idx = 0;
	for (size_t i = 0; i < len; i++) {
		for (uint8_t j = 0; j < 8; j++) {
			uint8_t b = (in[i] >> (7 - j)) & 0x1;
			for (uint16_t k = 0; k < 2047; k++) {
				/* i is on the MS bit of the spread sequence */
				sr = (sr << 1) | ((spread_seq[k] >> 7) ^ b);
				sr_cnt = (sr_cnt + 1) % 8;
				if (sr_cnt == 0) {
					out[out_idx++] = sr;
				}
			}
		}
	}
	return out_idx;
}

int
rildos_spread(uint8_t *out, const uint8_t *in, const uint8_t *spread_seq,
              size_t len, rildos_mod_t mod)
{
	if (!out || !in) {
		return -INVAL_PARAM;
	}

	if (mod == RILDOS_MOD_BPSK) {
		return spread_bpsk(out, in, spread_seq, len);
	}
	return -INVAL_PARAM;
}





